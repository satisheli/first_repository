package com.jpm.selenium.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ChromeUtil {
	public static WebDriver getChromeDriver(){
		WebDriver driver=null;
		//Step 1: driver class
		String driverClassKey="webdriver.chrome.driver";
		//Step 2: driver path (we have kept driver in driver folder)
		String driverPath=".\\driver\\chromedriver.exe";
		//step 3: set the system class properties.
		System.setProperty(driverClassKey, driverPath);
		//step 4: set the chrome opations
		ChromeOptions options = new ChromeOptions();
		//Step 5: get the chrome driver instance by passing ChromeOptions.
		driver = new ChromeDriver(options);
		System.out.println("Trying to load Chrome Browser...");
		
		if(driver!=null){
			System.out.println("Driver loaded!!!");
		}		
		return driver;
	}
	
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = ChromeUtil.getChromeDriver();
		System.out.println("Opening the Browser");
		System.out.println("Driver: " +driver);
		System.out.println("Closing the Browser");
		Thread.sleep(5000);
		driver.close();
	}

}
