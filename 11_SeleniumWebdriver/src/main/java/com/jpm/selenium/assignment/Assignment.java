package com.jpm.selenium.assignment;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.jpm.selenium.util.ChromeUtil;

public class Assignment {
	
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = ChromeUtil.getChromeDriver();
		String url = "http://demo.opencart.com/index.php?route=account/register";
		driver.get(url);
		
		driver.findElement(By.name("firstname")).sendKeys("Satish");
		driver.findElement(By.id("input-lastname")).sendKeys("Eli");
		driver.findElement(By.id("input-email")).sendKeys("satishe@gmail.com");
		driver.findElement(By.name("telephone")).sendKeys("8527419636");
		driver.findElement(By.name("password")).sendKeys("GitHub@123");
		driver.findElement(By.name("confirm")).sendKeys("GitHub@123");
		
		List<WebElement> radioElements = driver.findElements(By.name("newsletter"));
		for(WebElement radio: radioElements){
			String radioSelection=radio.getAttribute("value").toString();
			//which radio button to select
			if(radioSelection.equals("1")){
				radio.click();				
			}
		}
		
		driver.findElement(By.name("agree")).click();
		
		//*[@id="content"]/form/div/div/input[2]
		
		driver.findElement(By.xpath("//*[@id='content']/form/div/div/input[2]")).click();
		
		driver.findElement(By.xpath("//*[@id='content']/div/div/a")).click();
		
		
		
		
		
		
		
		Thread.sleep(2000);
		//driver.close();
	}
}
