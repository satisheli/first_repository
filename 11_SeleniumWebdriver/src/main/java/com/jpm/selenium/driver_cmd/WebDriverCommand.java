package com.jpm.selenium.driver_cmd;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.jpm.selenium.util.ChromeUtil;

public class WebDriverCommand {
	
	public static void main(String[] args) throws InterruptedException {
		// We need instance of driver
		WebDriver driver = ChromeUtil.getChromeDriver();
		if(driver!=null){
			//get -> to launch the website
			//String url = "https://getbootstrap.com";
			String url = "file:\\D:\\Satish\\Day-51\\11_SeleniumWebdriver\\src\\main\\java\\Locators.html";
			driver.get(url);
			//lets get the title of the webpage
			System.out.println("Title of the page: "+driver.getTitle());
			System.out.println("Current URL is: "+driver.getCurrentUrl());
			//By.id
			WebElement element=driver.findElement(By.id("user"));
			System.out.println("Element 1: "+element.getAttribute("id"));
			element.sendKeys("satisheli");
			
			//input password by name
			driver.findElement(By.name("password")).sendKeys("password@123");
			
			//By.linkText
			element= driver.findElement(By.linkText("How to use locators?"));
			System.out.println(("element link text: "+element.getText()));
			Thread.sleep(5000);
			element.click();//it will click the link as the WebElement is holding.
			
		}
		else{
			System.out.println("Driver not loaded!!!");
		}
		Thread.sleep(5000);
		driver.close();
	}

}
