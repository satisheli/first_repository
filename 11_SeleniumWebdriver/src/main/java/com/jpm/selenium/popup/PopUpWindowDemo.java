package com.jpm.selenium.popup;

import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.jpm.selenium.util.ChromeUtil;

public class PopUpWindowDemo {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = ChromeUtil.getChromeDriver();
		String url="file:\\D:\\Satish\\Day-51\\11_SeleniumWebdriver\\src\\main\\java\\PopUpWinDemo.html";
		driver.get(url);
		Thread.sleep(2000);
		
		String parentWin = driver.getWindowHandle();
		System.out.println("Parent Win: "+parentWin);
		Thread.sleep(2000);
		
		driver.findElement(By.id("newtab")).click();
		parentWin = driver.getWindowHandle();
		System.out.println("Parent Win: "+parentWin);
		Thread.sleep(2000);
		
		Set<String> childWinList = driver.getWindowHandles();//all the window used.
		for(String childWin: childWinList){
			if(!childWin.equals(parentWin)){
				//Thread.sleep(2000);
				driver.switchTo().window(childWin);
				System.out.println("Child Win: "+childWin);
				driver.findElement(By.id("alert")).click();
				Thread.sleep(2000);
				Alert alert=driver.switchTo().alert();
				//Thread.sleep(2000);
				alert.accept();//use dismiss() to cancel
			}
		}
		driver.switchTo().window(parentWin);
		System.out.println("Switch to Parent Win: "+parentWin);
		Thread.sleep(2000);
		driver.findElement(By.id("newwindow")).click();
		Thread.sleep(2000);
		
		for(String childWin2: driver.getWindowHandles()){
			if(!childWin2.equals(parentWin)){
				driver.switchTo().window(childWin2);
				Thread.sleep(2000);
				break;
			}
		}
		
		
		
			
		Thread.sleep(2000);
		driver.quit();
		//driver.close();
	}
}
